# AI 写的一个计时器示例
import tkinter as tk

def add():
    num1 = int(inp1.get())
    num2 = int(inp2.get())
    result = num1 + num2
    out.delete(0, tk.END)
    out.insert(0, result)

def subtract():
    num1 = int(inp1.get())
    num2 = int(inp2.get())
    result = num1 - num2
    out.delete(0, tk.END)
    out.insert(0, result)

# 创建主窗口
root = tk.Tk()
root.title("加减法计算器")
# 设置窗口大小和位置
root.geometry("300x150")
# 设置窗口位置
root.eval('tk::PlaceWindow . center')

# 创建两个输入框
inp1 = tk.Entry(root, width=10)
inp1.pack(pady=5)
inp2 = tk.Entry(root, width=10)
inp2.pack(pady=5)

# 创建两个按钮
btn_add = tk.Button(root, text="加", command=add)
btn_add.pack(pady=5)
btn_subtract = tk.Button(root, text="减", command=subtract)
btn_subtract.pack(pady=5)

# 创建一个输出框
out = tk.Entry(root, width=10)
out.pack(pady=5)

# 运行主循环
root.mainloop()
