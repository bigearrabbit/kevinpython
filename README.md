# Kt Python 学习教程

edit 20:50]
# chapter1
I want to study Python

```
print("Hello Python")
print("You are welcome!")
```
#### 介绍
Python 学习教程
Python的教程，包含多个工程目录。

# Hello.py
输出Hello Python

# 目录
[Wiki](https://gitee.com/bigearrabbit/KevinPython/wikis/Home)

[hello.py](md/hello.md)


# 安装 模块
tkinter:设计界面 不需要安装
安装：

```
# 安装 pyautogui
pip install pyautogui  -i https://pypi.tuna.tsinghua.edu.cn/simple

# 安装 pynput
pip install pynput -i https://pypi.tuna.tsinghua.edu.cn/simple
```


# 打包exe
`pip install PyInstaller -i https://pypi.tuna.tsinghua.edu.cn/simple`
运行：button_robot_package.bat


# Kevin的教程
<p>【Python动画设计教程】系列文章目录<br> 
<a href="https://blog.csdn.net/bigearrabbit/article/details/112055799">【第一讲】软件准备</a><br>
 <a href="https://blog.csdn.net/bigearrabbit/article/details/112637247">【第二讲】搭建界面，Excel读取，图片读取</a><br> 
 <a href="https://blog.csdn.net/bigearrabbit/article/details/112637637">【第三讲】图像绘制</a><br> 
 <a href="https://blog.csdn.net/bigearrabbit/article/details/112638301">【第四讲】制作动画</a></p>

