
# 安装方法见Readme.md
# 按键机器人
# 鼠标左键点击录制
# 按任意键结束录制
# 录制文件名：button_robot_clicks.txt
# 录制文件内容：鼠标点击坐标
# 录制文件内容格式：x,y

import tkinter as tk # 界面设计
import pyautogui # 模拟点击
import time # 延时
from pynput import mouse #监听鼠标
from pynput import keyboard # 监听键盘

# 不起作用
def check_listener(listener):
    # 获取监听器关联的线程
    thread = getattr(listener, '_thread', None)
    # 检查线程是否存在，以及线程是否活动
    return thread is not None and thread.is_alive()

# 监听鼠标和键盘的类
class MyListen:
    def __init__(self):
        self.start = 0 
        self.clicks = [] # 鼠标点击事件数组 
        self.listen_key = None
        self.listen_mouse = None
 
    # 键盘按下事件
    def on_down(self, key):
        if self.start == 1:
            self.start = 0
        else: 
            app.replay_start = 0
        self.stop_listening_mouse()
        self.stop_listening_keyboard()

    # 开始监听key
    def start_listening_keyboard(self):
        if self.listen_key is not None : self.listen_key.stop()
  
        print("---开始监听键盘---")  
        self.listen_key = keyboard.Listener(on_press=self.on_down)
        self.listen_key.start() 

    # 开始监听
    def start_listening_mouse(self): 
        if self.listen_mouse is not None : self.listen_mouse.stop()
        
        print("---开始监听鼠标---")  
        self.listen_mouse = mouse.Listener(on_click=self.on_m_click) 
        self.listen_mouse.start() 
   
        # 清空
        self.clicks = []
        self.start = 1

    # 关闭监听
    def stop_listening_mouse(self):
        if self.listen_mouse is not None :
            self.listen_mouse.stop()
            self.listen_mouse = None
            print("---停止监听鼠标---")
        self.start = 0

    # 停止监听key
    def stop_listening_keyboard(self):
        if self.listen_key is not None :
            self.listen_key.stop()
            self.listen_key = None
            print("---停止监听键盘---")

    # 鼠标点击事件
    def on_m_click(self, x, y, button, pressed): 
        if self.start != 1 : return False
        if pressed:             
            self.clicks.append((x, y))
            print(f"Mouse clicked at ({x}, {y}) with {button}")


# 主窗口类
class MyRobotWidget():
    def __init__(self):
        self.listen = MyListen()   # 监听
        self.root = tk.Tk()

        # ====初始化界面======
        self.root.title("鼠标机器人") 
        self.file_name = "button_robot_clicks.txt"
        self.replay_count = 100 # 回放次数
        self.replay_sleep = 100 # 休息的毫秒
        self.replay_start = 0 # 开始播放

        # 设置窗口位置
        self.root.eval('tk::PlaceWindow . center')

        iRow = 1

        # 创建两个输入框
        iRow = iRow + 1
        self.label_count = tk.Label(self.root, text ='播放次数:')
        self.label_count.grid(column = 0, row = iRow)
        self.entry_int = tk.Entry(self.root, width=25)
        self.entry_int.grid(column = 1, row = iRow, columnspan=2, padx=10, sticky="w")
        self.entry_int.insert(0, self.replay_count)

        # 点击间隙
        iRow = iRow + 1
        self.label_sleep = tk.Label(self.root, text ='间隙(毫秒):')
        self.label_sleep.grid(column = 0, row = iRow)
        self.entry_sleep = tk.Entry(self.root, width=25)
        self.entry_sleep.grid(column = 1, row = iRow, columnspan=2, padx=10, sticky="w")
        self.entry_sleep.insert(0, self.replay_sleep)

        # 文件名
        iRow = iRow + 1
        self.label_file = tk.Label(self.root, text ='文件名:')
        self.label_file.grid(column = 0, row = iRow)
        self.entry_file = tk.Entry(self.root, width=25)
        self.entry_file.grid(column = 1, row = iRow, columnspan=2, padx=10, sticky="w")
        self.entry_file.insert(0, self.file_name)

        iRow = iRow + 1
        self.label1 = tk.Label(self.root, text =' ')
        self.label1.grid(column = 1, row = iRow)

        # 创建按钮

        iRow = iRow + 1
        self.btn_read_file = tk.Button(self.root, text='读取坐标',
                                       command=self.clicks_read_file) 
        self.btn_read_file.grid(column = 0, row = iRow, padx=5, pady=5) 

        self.btn_save_file = tk.Button(self.root, text='保存坐标',
                                       command=self.clicks_save_file) 
        self.btn_save_file.grid(column = 1, row = iRow)  

        self.btn_print = tk.Button(self.root, text='打印坐标',
                                   command=self.clicks_print) 
        self.btn_print.grid(column = 2, row = iRow, padx=5, pady=5)  
        

        iRow = iRow + 1
        self.btn_read_file = tk.Button(self.root, text='录制',
                                       command=self.start_record) 
        self.btn_read_file.grid(column = 0, row = iRow, padx=5, pady=5) 

        self.label_title = tk.Label(self.root, text ='按任意键结束录制!')
        self.label_title.grid(column = 1, row = iRow, columnspan=2, sticky="w")
        
        iRow = iRow + 1
        self.btn_replay = tk.Button(self.root, text='播放', 
                                    command=self.clicks_replay)
        self.btn_replay.grid(column = 0, row = iRow, padx=10, pady=5) 
        label = tk.Label(self.root, text ='鼠标移到角落 （或任意键）：停止播放!')
        label.grid(column = 1, row = iRow, columnspan=2, sticky="w")


    def quit(self):
       self.root.destroy()

    # 开始录制
    def start_record(self):
        print ("---start_record()---")
        self.listen.start_listening_mouse()
        self.listen.start_listening_keyboard() 
       
    def UpdateInfo(self):
        print ("---UpdateInfo()---")
       
        # 获得entry_int的int值  
        self.replay_count = int(self.entry_int.get())
        if(self.replay_count < 1): self.replay_count = 1
        print (f"   播放次数 = {self.replay_count}---")

        # 获得entry_sleep  
        self.replay_sleep = int(self.entry_sleep.get())
        if(self.replay_sleep <= 5 ): self.replay_sleep = 5
        print (f"   间隔 = {self.replay_sleep}毫秒---")

        self.file_name = self.entry_file.get()
        print (f"   文件名   = {self.file_name}---")


    # 读取点击坐标
    def clicks_read_file(self):
        self.listen.clicks = []
        
        self.UpdateInfo() # 获得界面值 
        if len(self.file_name) < 3 :
            print(" {NG} 文件名不能为空")
            return

        fileName = f"{self.file_name}"
        with open(fileName, 'r') as f:
            for line in f.readlines():
                str1 = line.strip()
                if len (str1) <3: continue
                coords = str1.split(',')
                if len(coords) < 2: continue
                self.listen.clicks.append((int(coords[0]),int(coords[1])))
            f.close()
        print(f"   从文件读取坐标: {fileName}, 点数 = {len(self.listen.clicks)}---")

    # 保存点击坐标
    def clicks_save_file(self): 
        print("---保存坐标到文件---")  
        count = len(self.listen.clicks) 
        if count < 1: 
            print(" {NG} 没有坐标")
            return
        
        self.UpdateInfo() # 获得界面值 
        if len(self.file_name) < 3 :
            print(" {NG} 文件名不能为空")
            return

        with open(self.file_name, 'w') as f:
            # 写入数组
            for click in self.listen.clicks: 
                f.write(f"{click[0]},{click[1]}\n") 
            f.close()
        print(f"   Save to file = {self.file_name}, count = {count}---")

    # 打印点击事件
    def clicks_print(self):
        print("---打印坐标---")

        if len(self.listen.clicks) < 1: 
            print(" {NG} 没有坐标")
            return
        
        # 遍历数组 
        for click in self.listen.clicks: 
            print(f"{click[0]},{click[1]}")
                
    # 回放点击事件
    def clicks_replay(self):
        if len(self.listen.clicks) < 1: 
            print(" {NG} 没有坐标")
            return
        
        self.UpdateInfo() # 获得界面值 

        # 循环点击
        i = 0
        i = 0
        
        print("   点坐标： =")
        for click in self.listen.clicks:
            print(f"{i}\t,{click[0]},{click[1]}")
            i=i+1

        # 循环点击    
        print(f"循环模拟点击, sleep = {self.replay_sleep} ms:")
        i = 0
        
        self.listen.start_listening_keyboard() # 监听键盘
        self.replay_start = 1 # 开始播放

        # 循环： 检查退出条件
        while i < self.replay_count and self.replay_start == 1:
            # 输出次数
            i = i+1
            print(i)

            for click in self.listen.clicks:
                time.sleep(self.replay_sleep * 0.001)
                pyautogui.click(click[0],click[1])
                if self.replay_start != 1: break # 退出
                
        self.listen.stop_listening_keyboard() # stop


# 创建主窗口
app = MyRobotWidget()

# 运行主循环
app.root.mainloop()
